# HD44780 class for micropython board (http://micropython.org)
# Written by Will Pimblett, based on http://www.raspberrypi-spy.co.uk/2012/07/16x2-lcd-module-control-using-python/
# http://github.com/wjdp/micropython-lcd
# http://wjdp.co.uk
# Modified to work with the ESP8266
# Pinout set to work with Wemos D1 Mini <https://wiki.wemos.cc/products:retired:d1_mini_v2.2.0>

from machine import Pin
import time

class HD44780(object):
    # Pinout, change within or outside class for your use case

    # D0 (GPIO16)   ------>     RS
    # D1 (GPIO5)    ------>     E
    # D2 (GPIO4)    ------>     
    # D3 (GPIO0)    ------>     
    # D4 (GPIO2)    ------>     DB4
    # D5 (GPIO14)   ------>     DB5
    # D6 (GPIO12)   ------>     DB6
    # D7 (GPIO13)   ------>     DB7
    # D8 (GPIO15)   ------>     

    PINS = [16, 5, 2, 14, 12, 13]
    # Pin names, don't change
    PIN_NAMES = ['RS','E','D4','D5','D6','D7']

    # Dict of pins
    pins = {}

    # GPIO pin mode output
    PIN_MODE = Pin.OUT

    # Define some device constants
    LCD_WIDTH = 16    # Maximum characters per line
    # Designation of T/F for character and command modes
    LCD_CHR = True  # RS = 1
    LCD_CMD = False # RS = 0

    LINES = {
        0: 0x80, # LCD RAM address for the 1st line
        # DDRAM Addr. = 0x40 | 0b10000000
        1: 0xC0, # LCD RAM address for the 2nd line
        # Add more if desired
    }

    # Timing constants
    E_PULSE = 50
    E_DELAY = 50

    def init_gpio(self):
        # Initialise pins
        for pin, pin_name in zip(self.PINS, self.PIN_NAMES):
            # setattr(self, 'LCD_'+pin_name,   # Unsupported
            #     machine.Pin(pin, self.PIN_MODE))
            self.pins['LCD_'+pin_name] = Pin(pin, self.PIN_MODE)

    def lcd_init(self):
        self.lcd_byte(0b00110011, self.LCD_CMD)
        self.lcd_byte(0b00110010, self.LCD_CMD)
        self.lcd_byte(0b00000110, self.LCD_CMD)
        self.lcd_byte(0b00001100, self.LCD_CMD)
        self.lcd_byte(0b00101000, self.LCD_CMD)
        self.lcd_byte(0b00000001, self.LCD_CMD)
        time.sleep_ms(2)

    def clear(self):
        # Clear the display
        self.lcd_byte(0x01,self.LCD_CMD)

    def set_line(self, line):
        # Set the line that we're going to print to
        self.lcd_byte(self.LINES[line], self.LCD_CMD)

    def set_string(self, message):
        # Pad string out to LCD_WIDTH
        # message = message.ljust(LCD_WIDTH," ")
        m_length = len(message)
        if m_length < self.LCD_WIDTH:
            short = self.LCD_WIDTH - m_length
            blanks=str()
            for i in range(short):
                blanks+=' '
            message+=blanks
        for i in range(self.LCD_WIDTH):
            self.lcd_byte(ord(message[i]), self.LCD_CHR)

    def lcd_byte(self, bits, mode):
        # Send byte to data pins
        # bits = data
        # mode = True  for character
        #        False for command

        self.pin_action('LCD_RS', mode) # RS

        # High bits
        self.pin_action('LCD_D4', False)
        self.pin_action('LCD_D5', False)
        self.pin_action('LCD_D6', False)
        self.pin_action('LCD_D7', False)
        if bits&0x10==0x10:
            self.pin_action('LCD_D4', True)
        if bits&0x20==0x20:
            self.pin_action('LCD_D5', True)
        if bits&0x40==0x40:
            self.pin_action('LCD_D6', True)
        if bits&0x80==0x80:
            self.pin_action('LCD_D7', True)

        # Toggle 'Enable' pin
        self.udelay(self.E_DELAY)
        self.pin_action('LCD_E', True)
        self.udelay(self.E_PULSE)
        self.pin_action('LCD_E', False)
        self.udelay(self.E_DELAY)

        # Low bits
        self.pin_action('LCD_D4', False)
        self.pin_action('LCD_D5', False)
        self.pin_action('LCD_D6', False)
        self.pin_action('LCD_D7', False)
        if bits&0x01==0x01:
            self.pin_action('LCD_D4', True)
        if bits&0x02==0x02:
            self.pin_action('LCD_D5', True)
        if bits&0x04==0x04:
            self.pin_action('LCD_D6', True)
        if bits&0x08==0x08:
            self.pin_action('LCD_D7', True)

        # Toggle 'Enable' pin
        self.udelay(self.E_DELAY)
        self.pin_action('LCD_E', True)
        self.udelay(self.E_PULSE)
        self.pin_action('LCD_E', False)
        self.udelay(self.E_DELAY)

    def udelay(self, us):
        # Delay by us microseconds, set as function for portability
        time.sleep_us(us)

    def pin_action(self, pin, high):
        # Pin high/low functions, set as function for portability
        if high:
            self.pins[pin].on()
        else:
            self.pins[pin].off()
