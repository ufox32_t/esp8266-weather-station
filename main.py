# main.py -- put your code here!

import machine
import ntptime
import time
import urequests
from lcd import HD44780
from micropython import const

DEGREE_SIGN     = const(223)
LCD_LINE_0      = const(0x00)
LCD_LINE_1      = const(0x40)
LCD_LINE_2      = const(0x14)
LCD_LINE_3      = const(0x54)

lcd = HD44780()

def main(args):

    # set the CPU frequency to 160 MHz
    machine.freq(160000000)
    
    # initialize the LCD display
    lcd.init_gpio()
    lcd.lcd_init()

    lcd_write_string("starting up...")

    # synchronize the real time clock 
    ntptime.host = "ptbtime1.ptb.de"
    ntptime.settime()

    time_previous_exec_ms = 0
    weather_previous_exec_ms = 0

    while True:

        lcd_line_0_buff = update_time_text()
        time_previous_exec_ms = time.ticks_ms()
        weather_data_text = update_weather_text()
        weather_previous_exec_ms = time.ticks_ms()
        
        # clear display & write text on the display
        lcd.lcd_byte(0x01, lcd.LCD_CMD)
        time.sleep_ms(2)    
        lcd_write_string(lcd_line_0_buff)
        lcd.lcd_byte(LCD_LINE_2 | 0x80, lcd.LCD_CMD)
        lcd_write_string(weather_data_text['weather_str_line_0'])
        lcd.lcd_byte(LCD_LINE_3 | 0x80, lcd.LCD_CMD)
        lcd_write_string(weather_data_text['weather_str_line_1'])

        time.sleep(10)

    return 0

def lcd_write_string(string):
    for c in string:
        lcd.lcd_byte(ord(c), lcd.LCD_CHR)

def update_weather_text():
    # get weather data from OpenWeatherMap API <https://openweathermap.org/current>
    # get your own key at <https://home.openweathermap.org/users/sign_up>
    r = urequests.get("http://api.openweathermap.org/data/2.5/weather?id=2518794&units=metric&APPID=********")
    data = r.json()
    r.close()

    temp = data['main']['temp']
    pressure = data['main']['pressure']
    humidity = data['main']['humidity']
    wind_speed = data['wind']['speed']
    wind_deg = data['wind']['deg']
    
    weather_str_line_0 = str(temp) + chr(DEGREE_SIGN) + 'C ' + str(humidity) + '% ' + str(pressure) + 'hPa'
    weather_str_line_1 = 'Wind: ' + str(wind_speed) + 'm/s ' + str(wind_deg) + chr(DEGREE_SIGN)

    return {'weather_str_line_0' : weather_str_line_0, 'weather_str_line_1' : weather_str_line_1}

def update_time_text():
    # gets local time and returns a formated text string

    local_time = time.localtime()

    t_year = local_time[0]
    t_mon = local_time[1]
    t_mday = local_time[2]
    t_hour = local_time[3] + 1
    t_min = local_time[4]
    
    lcd_time_str = str(t_hour) + ':'
    if t_min < 10:
        lcd_time_str += '0'
    lcd_time_str += str(t_min) + ' - '
    if t_mday < 10:
        lcd_time_str += '0'
    lcd_time_str += str(t_mday) + '/'
    if t_mon < 10:
        lcd_time_str += '0'
    lcd_time_str += str(t_mon) + '/' + str(t_year)
    return lcd_time_str

if __name__ == '__main__':
    import sys
    sys.exit(main(sys.argv))